#!/usr/bin/env bash

docker run -it --entrypoint /bin/bash --privileged \
     -v /sys/class/leds/led1/brightness:/sys/class/leds/led1/brightness \
     -v /sys/class/leds/led1/trigger:/sys/class/leds/led1/trigger \
     -v /sys/class/leds/led0/brightness:/sys/class/leds/led0/brightness \
     -v /sys/class/leds/led0/trigger:/sys/class/leds/led0/trigger \
     ledice
