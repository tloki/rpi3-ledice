#!/usr/bin/env python3

import os

def green_enable():
    os.system("echo none | tee /sys/class/leds/led0/trigger >> /dev/null")

def green_on():
    # set cotrol
    # os.system("echo none | tee /sys/class/leds/led0/trigger >> /dev/null")
    os.system("echo 1 | tee /sys/class/leds/led0/brightness >> /dev/null")

def green_off():
    # os.system("echo none | tee /sys/class/leds/led0/trigger >> /dev/null")
    os.system("echo 0 | tee /sys/class/leds/led0/brightness >> /dev/null")

def red_enable():
    os.system("echo none | tee /sys/class/leds/led1/trigger >> /dev/null")

def red_on():
    # set cotrol
    # os.system("echo none | tee /sys/class/leds/led0/trigger >> /dev/null")
    os.system("echo 1 | tee /sys/class/leds/led1/brightness >> /dev/null")

def red_off():
    # os.system("echo none | tee /sys/class/leds/led0/trigger >> /dev/null")
    os.system("echo 0 | tee /sys/class/leds/led1/brightness >> /dev/null")


PER = 0.5

if __name__ == "__main__":
    import time
    #control(red_led, "reset")
    
    green_enable()
    red_enable()
    
    while(True):
        red_on()
        green_on()
        time.sleep(PER/2)
        green_off()
        red_off()
        time.sleep(PER/2)
